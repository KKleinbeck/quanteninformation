% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US

For the transmon qubit in the fitting regime we can write the hamiltonian in the
two level approximation
\begin{equation*}
	H = - \frac{E_J}{2} \sigma_z - \frac{E_{el}}{2}\sigma_x,
\end{equation*}
where we defined $E_{el} = 4E_C(1-2n_g)$. We had for $n_g = \frac{C_gV_g}{2e}$ with $C_g$ the capacity of the
coupling capacitor. and $V_g = V_g^{ex} + V_{LC}$ the voltage at it. This voltage is composed of the external source
voltage $V_g^{ex}$ and the voltage of the LC-circuit $V_{LC}$. The quantum fluctuations are in the LC-circuit voltage,
so we have a term $\propto V_{LC} \sigma_X$ in the hamiltonian with random fluctuations.

So by coupling the transmon to the LC-circuit we get
\begin{align*}
	H &= H_{LC} + H_{q} + H_{int}\\
	&= \omega_r a^\dagger a + \omega_q \sigma_z + g \sigma_x (a^\dagger + a).
\end{align*}
The parameter $g$ describes the interaction energy between the transmon and the LC-circuit. Since it is a fluctuating
quantity we express it in term of statistical values, here the root mean square,
\begin{equation*}
	g = \frac{e}{\hbar}\left[\frac{C_g}{C_t}\right]V_{rms}^0.
\end{equation*}
$C_t$ describes the total capacity of the superconducting box. $g$ is at the order of magnitude \SI{100}{\mega\hertz},
where the energies $\omega_c$ and $\omega_q$ are at \SI{5}{\giga\hertz} and \SI{1}{\giga\hertz}. We apply the
rotating wave approximation, where $\omega_r + \omega_q \gg \omega_r - \omega_q \gg g$. Here we may approximate the
interaction hamiltonian with
\begin{equation*}
	H_{int} = \sigma^+ a + \sigma^-a^\dagger.
\end{equation*}
The new single qubit hamiltonian
\begin{equation*}
	H = \omega_r a^\dagger a + \omega_q \sigma_z + g(\sigma^+ a + \sigma^-a^\dagger)
\end{equation*}
is now exactly solvable.

We can now think of single qubit rotations on the bloch sphere, i.e., the qubit gates. For this we will have a look at
how the Hamiltonian behaves under coupling to a electro magnetic wave
\begin{equation*}
	H = \omega_r a^\dagger a + \omega_q \sigma_z + g(\sigma^+ a + \sigma^-a^\dagger)
		+ \sum_k\left(\varepsilon_k(t) e^{i\omega_kt}a^\dagger + \varepsilon_k^*(t)e^{-i\omega_kt}a \right),
\end{equation*}
where $k$ labels the different wave modes. We apply now a transformation
\begin{equation*}
	D(\alpha) = e^{\alpha a^\dagger - \alpha^* a},
\end{equation*}
with a time-dependent parameter $\alpha$. Under the condition
\begin{equation*}
	\dot{\alpha} = -i\omega_r\alpha - i \varepsilon(t) e^{-i\omega_kt},
\end{equation*}
the transformed Hamiltonian reads
\begin{align*}
	\tilde{H} &= D^\dagger(\alpha) H D(\alpha) - iD^\dagger(\alpha)D(\alpha) \\
	&= \Delta_r a^\dagger a + \frac{\Delta_q}{2} \sigma_z - g(\sigma^+ a + \sigma^-a^\dagger)
		+ \frac{\Omega_R}{2} \sigma_x,
\end{align*}
where we defined
\begin{align*}
	\Delta_r &= \omega_r - \omega_d \\
	\Delta_q &= \omega_q - \omega_d \\
	\Omega_R &= \frac{2g\varepsilon}{\Delta_r}.
\end{align*}
This result is limited to just one mode in the driving field, with amplitude $\varepsilon$ and frequency $\omega_d$.

We now want to create a bit flip gate, i.e., a $X$-gate. We have to get rid of the $\propto g$ term in the
Hamiltonian, so that our only interaction is a pure rotation. For this we will need a second transformation
\begin{equation*}
	U = e^{\frac{g}{\Delta_r}(\sigma^+a - \sigma^-a^\dagger)}.
\end{equation*}
Now, after the second transformation, we end up with
\begin{equation*}
	\hat{H} = U \tilde{H}U^\dagger.
\end{equation*}
For the computation of this expression we need the Baker-Campel-Hausdorff formula
\begin{equation*}
	e^{-\lambda O} H e^{\lambda O} = H + \lambda[H,O] + \frac{\lambda^2}{2} \big[[H,O],O \big] + \dots.
\end{equation*}
This expansion is valid, since $g/\Delta_r = \mathcal{O}(10^{-3})$.
We get
\begin{equation*}
	\hat{H} \approx \Delta_r a^\dagger a + \frac{\tilde{\Delta}_q}{2} \sigma_z + \frac{\Omega_R}{2} \sigma_x,
\end{equation*}
where the only changed parameter is
\begin{equation*}
	\tilde{\Delta}_q
		= \frac{1}{2}\left(\Delta_q + \frac{2g^2}{\Delta^r}\left(a a^\dagger + \frac{1}{2}\right) \right).
\end{equation*}
A bit flip is now possible if $\Omega_R \gg \tilde{\Delta}_q$. For a perfect bit flip we would need
$\tilde{\Delta}_q = 0$, which is not possible, so in this gates we will never have a fidelity of $1$, there is a
upper limit.

For a phase gate, i.e., $Z$-gate, we need to make $\Delta_q$ big and $\Omega_R$ small. Again we apply another
transformation, bringing us to
\begin{align*}
	H' &= U\hat{H} U^\dagger,
		\qquad U =  e^{\beta\sigma^+ - \beta^*\sigma^-}, \quad\beta = \frac{\Omega_R}{2\Delta_q}\\
		&\approx \Delta_r a^\dagger a
			+ \frac{1}{2}\left(\tilde{\Delta}_q + \frac{1}{2}\frac{\Omega_R^2}{\Delta_q}\right) \sigma_z.
\end{align*}

We can now understand gates on this qubit system in terms of these transformations.
For example we have
\begin{equation*}
	\kup + \kdown \overset{U_z = e^{iH_zt}}{\longrightarrow} \kup + e^{i\phi}\kdown,
		\qquad \phi = \frac{1}{2} \left(\frac{\Omega_R^2}{\Delta_q}\right)t
\end{equation*}
and
\begin{equation*}
	\kup \overset{U_x = e^{iH_xt}}{\longrightarrow} \cos\theta\kup + i\sin\theta\kdown,
		\qquad \theta = \frac{\Omega_R}{2}t.
\end{equation*}

Now, for the two qubit system we have
\begin{equation*}
	H_D = \omega_r a^\dagger a + \omega_{q_1} \sigma_{z_1} + g_{1}(\sigma^+_1 a + \sigma^-_1a^\dagger)
		+ \omega_{q_2} \sigma_{z_2} + g_2(\sigma^+_2 a + \sigma^-_2a^\dagger).
\end{equation*}
Here, by applying
\begin{equation*}
	U = \exp\left[\frac{g_1}{\Delta_1}(a^\dagger \sigma^-_1 - a\sigma^+_1)
		+ \frac{g_2}{\Delta_2}(a^\dagger \sigma^-_2 - a\sigma^+_2) \right],
\end{equation*}
where $\Delta_i = \omega_r - \omega_{z_i}$, we get
\begin{equation*}
	\tilde{H}_D = \omega_r a^\dagger a + \tilde{\omega}_{q_1}\sigma_{z_1} + \tilde{\omega}_{q_2}\sigma_{z_2}
		+ \frac{g_1 g_1 (\Delta_1 + \Delta_2)}{2\Delta_1\Delta_2}(\sigma_1^+\sigma_2^- + \sigma_1^-\sigma_2^+).
\end{equation*}
We see, there is a coupling between both qubits in this form.

Now we want to create a CNOT gate in this system. Remember: We showed that by having a CNOT gate and arbitrary
rotations we can build up every gate. We first show, how we can build a SWAP gate. In the case of
$\omega_{q_1} = \omega_{q_2}$ the transformation generating the SWAP is just $U_{sw}(t) = e^{i\tilde{H}_D t}$.
For $U(\tau)$ with $\tau = \frac{\pi}{2g_{\mathrm{eff}}}$,
$g_{\mathrm{eff}} = \frac{g_1 g_1 (\Delta_1 + \Delta_2)}{2\Delta_1\Delta_2}$, we have $U(\tau) = \text{SWAP}$.
For further reference have a look at the two Physical Review A papers \textit{75}, 032329 (2007) and
\textit{69}, 062320 (2004).































