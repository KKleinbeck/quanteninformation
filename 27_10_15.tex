% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US

Let us say, we can write
\begin{align}
	\notag
	\ket{\Psi_{AB}} &= \ket{\varphi_A} \otimes\ket{\chi_B}\\
	\notag
	&= \left(\alpha_1\ket{0} + \beta_1\ket{1}\right)+\left(\alpha_2\ket{0} + \beta_2\ket{1}\right)\\
	&= \alpha_1\alpha_2\ket{00} + \alpha_1\beta_2\ket{01} + \alpha_2\beta_1\ket{10} + \beta_1\beta_2\ket{11}.
	\label{1.6 eq:product state}
\end{align}
We see, there are correlations between the coefficients of the state. We can derive the condition
\begin{equation}
	C = |\alpha\delta - \beta\gamma|.
	\label{1.6 eq:independance condition}
\end{equation}
If $C=0$ then we can write the state in as a product state as in \eqref{1.6 eq:product state}. Now we look at a
state
\begin{align*}
	\ket{\Psi_{AB\dots Z}} &= \ket{\Psi_{AB}}\otimes \ket{\Psi_{C\dots Z}}\\
	&= \ket{\Psi_{A\dots E}} \otimes\ket{\Psi_{F\dots Z}},
\end{align*}
which we will call a bipartite system. It is not easy to find the conditions whether you can write such a state as
a product or not.


\section{Density Matrix}
In a closed system we can describe it with a wave function $\ket{\Psi}$. This description is not always useful, for
example if we don't have all informations about the system or we are looking at a subsystem. For example our source can
be faulty and if we prepare it in a state $\kPsi$ it has a given uncertainty. Now we use the transformation $U$ which
creates a new state $\kphi$. Since we cannot be sure, that our state was perfectly in $\kPsi$, we cannot be sure to
measure always $\kphi$.

Such an system can be described by the \emph{density matrix}
\begin{equation}
	\varrho = \sum_i c_i \varphi_1 = \sum_i c_i \ket{\Psi_i}\bra{\Psi_i}.
	\label{1.7 eq:Density matrix}
\end{equation}
For a pure state (we come to that later) it can be written as
\begin{equation*}
	\varrho = \kPsi\bPsi
\end{equation*}
\begin{itemize}
	\item $\varrho$ is a positive definite matrix.
	
	\item $\operatorname{Tr}\varrho = 1$.
	
	\item $\operatorname{Tr}\varrho^2 \le 1$. $\operatorname{Tr}\varrho$ is equal to 1, only if $\sum_i c_i^2 = 1$.
		If this condition is fulfilled we call the system pure. In the other case it's called mixed.
\end{itemize}
For Example
\begin{align*}
	\varrho &= c_1 \kz\bz + c_2 \ko\bo \\
	\varrho^2 &= c_1^2 \kz\bz + c_2^2 \ko\bo .
\end{align*}

How does this help us to understand subsystems? We look at the density matrix
\begin{equation}
	\varrho_{AB} = \ket{\Psi_{AB}}\bra{\Psi_{AB}},
\end{equation}
and we try to find the reduced density matrix $\varrho_A$. All possible states could be $\kzz$, $\kzo$, $\koz$ and
$\koo$, so $B$ could be $\kz$ or $\ko$. So we can get $\varrho_A$ be summing out $B$
\begin{align*}
	\varrho_A &= \operatorname{Tr}_B [\varrho_{AB}] \\
	&= \braket{0_B}{\Psi_{AB}}\braket{\Psi_{AB}}{0_B} + \braket{1_B}{\Psi_{AB}}\braket{\Psi_{AB}}{1_B}.
\end{align*}
It is not important in which base you are summing over $\varrho_{AB}$.

Let's have an example. We use the state
\begin{equation*}
	\ket{\Psi_{AB}} = \alpha\kzz + \beta\kzo + \gamma\koz + \delta\koo
\end{equation*}
and calculating the scalar products
\begin{align*}
	\braket{0_B}{\Psi_{AB}} \braket{\Psi_{AB}}{0_B} &=
		\left(\alpha\ket{0_A} + \gamma\ket{1_A}\right)\left(\alpha^*\bra{0_A} + \gamma^*\bra{1_A}\right) \\
%
	&= |\alpha|^2 \ket{0_A}\bra{0_A} + |\gamma|^2 \ket{1_A}\bra{1_A} \\
	&\phantom{=} + \alpha\gamma^* \ket{0_A}\bra{1_A} + \alpha^*\gamma \ket{1_A}\bra{0_A}.
\end{align*}
We do the same for $\ket{1_B}$ and we get
\begin{align*}
	\varrho_A &= \left(|\alpha|^2 + |\beta|^2\right) \ket{0_A}\bra{0_A}
		+ \left(|\gamma|^2 + |\delta|^2\right) \ket{1_A}\bra{1_A} \\
	&\phantom{=} \left(\alpha\gamma^* + \beta^*\delta \right) \ket{0_A}\bra{1_A}
		+ \left(\alpha^*\gamma + \beta^*\delta \right) \ket{1_A}\bra{0_A}.
\end{align*}
Were we see, that $\operatorname{Tr}\varrho_A = 1$ since $\left(|\alpha|^2 + |\beta|^2\right) =
\left(|\gamma|^2 + |\delta|^2\right) = 1$.


\section{Two-qubit unitaries}
The question we're asking is how to create a unitary transformation $U_{AB}$ which will transform a state
$\ket{\Psi_{AB}}$ into the state $\ket{\varphi_{AB}}$. As we have seen in the last section we cannot write
$\ket{\Psi_{AB}} = \ket{\Psi_A} \otimes \ket{\Psi_B}$, so, by the same arguments, we cannot write
$U_{AB} = U_A \otimes U_B$. If we can write $U_{AB}$ as such a product we call it a local unitary, since $U_A$ only
acts on $\ket{\Psi_A}$ and $U_B$ only acts on $\ket{\Psi_B}$. A non-local unitary acts on $\ket{\Psi_A}$ and
$\ket{\Psi_B}$ simultaneously.

If, for example,
\begin{equation*}
	Uf_{AB} = \ket{0_A}\bra{0_A} \otimes \mathbb{1}_B + \ket{1_A}\bra{1_A}\otimes \hat{X}_B,
\end{equation*}
we have a non-local unitary. This unitary transformations leaves $\ket{\Psi_B}$ untouched if $\ket{\Psi_A} = \kz$ and
flips $\ket{\Psi_B}$ if $\ket{\Psi_A} = \ko$. Again, we look at an example. We start in
\begin{equation*}
		\ket{\Psi} = \alpha\ket{00} + \beta\ket{01} + \gamma\ket{10} + \delta\ket{11}
\end{equation*}
and use the transformation
\begin{equation*}
	U_{AB} = \hat{X}_A\otimes\hat{X}_B.
\end{equation*}
Then we end in the state
\begin{equation*}
	U_{AB}	\ket{\Psi} = \alpha\ket{00} + \beta\koz + \gamma\kzo + \delta\ket{11}.
\end{equation*}
We see, that the local operation leaves $C = |\alpha\delta - \beta\gamma|$ untouched. Now we have a look at the
flip operator $Uf_{AB}$ we defined above. Here we end in
\begin{equation*}
	Uf_{AB}\ket{\Psi}  = \alpha\ket{00} + \beta\ket{01} + \gamma\ket{11} + \delta\ket{10},
\end{equation*}
which can change $C$.










