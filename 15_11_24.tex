% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US

\chapter{Measurements}
\section{projective measurements}

What do we measure? We measure an observable. What is an observable? Observables are
hermitian operators acting on the quantum system. What are the characteristics of hermitian operators? Hermitian
operators have real eigenvalues and orthogonal eigenvectors. What is known by measuring an observable?
You can find the probability of finding the quantum state to be in one of its eigenstates.

Example: We have the Qubit observable
\begin{equation*}
	\hat{X} =
	\left(\begin{matrix}
		0 & 1 \\ 1 & 0
	\end{matrix}\right).
\end{equation*}
This operator has the spectral decomposition
\begin{equation*}
	\hat{X} = (+1)\ket{v_1}\bra{v_1} + (-1)\ket{v_2}\bra{v_2},
\end{equation*}
with
\begin{align*}
	\ket{v_1} &= \frac{1}{\sqrt{2}}\left(\kz + \ko \right),\\
	\ket{v_2} &= \frac{1}{\sqrt{2}}\left(\kz-\ko\right).
\end{align*}

If we have a state $\kPsi = \alpha\kz + \beta\ko$ we can decompose it in the eigenbasis of the operator, i.e.\ 
$\kPsi = \gamma\ket{v_1} + \delta\ket{v_2}$. The probability of $\kPsi$ to be in the state $\ket{v_1}$ is
$|\gamma|^2$, but once collapsed, the system stays in this state, until it's changed.

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\draw (0,0) node[anchor=east] {S$_{1/2}$} -- (1,0) node[anchor=west] {$\kz$};
		\draw (0,2.5) node[anchor=east] {P$_{1/2}$} -- (1,2.5);
		\draw (2,2) node[anchor=east] {D$_{5/2}$} -- (3,2) node[anchor=west] {$\ko$};
		
		\draw[->] (2.5,2) -- (0.5,0);
		\draw[->] (0.5,0) -- (0.5,2.5) node[midway,anchor=south west,] {Laser};
		\draw[->] (0.3,2.5) -- (0.3,0);
		\draw[photong1] (0.3,1.25) -- (-0.5,1.25) node[midway,anchor=south,] {$\hbar\omega$};
	\end{tikzpicture}
	\caption{Measurement procedure in the ionic trap. We have chosen the S$_{1/2}$ state to be the ground
		state and D$_{5/2}$ to be the exited state. A laser is permanently pumping atoms from the ground state
		in the state P$_{1/2}$ from where it can relax into the ground state by emitting a photon. The
		measured light intensity then shows us how many atoms are in the ground state.}
	\label{6.1 fig:measure ionic trap}
\end{figure}

We now define a projective measurement operator as
\begin{equation*}
	\hat{M} = \sum m \hat{P}_{m},
\end{equation*}
where $\hat{P}_m$ are projective operators. The fulfill the relations
\begin{align*}
	\hat{a}^2_m &= \hat{P}_m \\
	\mathbb{1} &= \sum \hat{P}_m \\
	\left\{P_i, P_j \right\} &= \delta_{ij} P_i.
\end{align*}
The first to relations are also called completeness relations.
In a two state system they are defined as
\begin{align*}
	P_0 &= \kz\bz \\
	P_1 &= \ko\bo \\
	P_+ &= \frac{1}{2}\left(\kz + \ko\right)\left(\bz + \bo\right)\\
	P_- &= \frac{1}{2}\left(\kz - \ko\right)\left(\bz - \bo\right)
\end{align*}

With that we can define \emph{distinguishability}. We choose two states $\kPsi$ and $\kPhi$ at random. If they
fulfill
\begin{equation*}
	\braket{\Phi}{\Psi} = 0,
\end{equation*}
we call them perfectly distinguishable. If the scalar product results in a non-zero value we call both states
indistinguishable. With only the two projection operators $P_0$ and $P_1$ we cannot determine if any two states
are distinguishable in general (for example $\ko$ and $\kz+\ko$). We need one of the two operators $P_+$ and
$P_-$ as well.

\section{Positive-Operator-Values Measure}
A positive operator only has positive eigenvalues. One example we already know is the density matrix.
Now we choose a 3 dimensional vector with
\begin{equation*}
	\sum_{a} \lambda_a \bm{v}_a = 0,
\end{equation*}
and the constrains:
\begin{align*}
	0 < \lambda_a &< 1, \\
	\sum_{a}\lambda_a &= 1.
\end{align*}
We then write
\begin{align*}
	F_a &= \lambda_a\left(\mathbb{1} + \bm{v}_a\bm{\sigma} \right)\\
	&= (2\lambda_a) \underset{\text{density operator}}{
		\underbrace{\frac{1}{2}\left(\mathbb{1} + \bm{v}_a\bm{\sigma} \right)}}.
\end{align*}
By this we can show
\begin{equation*}
	\sum_a F_a = \mathbb{1},
\end{equation*}
i.e.\ $F_a$ is a projection operator for pure states. We found a method to build identity operators.

Example:
\begin{align*}
	\ket{\Psi_1} &= \kz\\
	\ket{\Psi_2} &= \frac{1}{\sqrt{2}}\left(\kz + \ko\right).
\end{align*}
Problem: find a positive operator value measure to distinguish between $\ket{\Psi_1}$ and $\ket{\Psi_2}$.
We choose $\lambda_a = c$ and $\bm{v}_1 = (0,0,1)$, $\bm{v}_2 = (1,0,0)$ and $\bm{v}_3 = (-1,0,-1)$, so
we fulfill the condition
\begin{equation*}
	\sum_a \lambda_a \bm{v}_a = 0.
\end{equation*}
Thus we have
\begin{align*}
	F_1 &= c \kz\bz\\
	F_2 &= c \ko\bo\\
	F_3 &= c (\kz + \ko)(\bz + \bo).
\end{align*}
For two states from above this yields in
\begin{align*}
	\bra{\Psi_1}F_1\ket{\Psi_1} &= c,\\
	\bra{\Psi_2}F_3\ket{\Psi_2} &= c,\\
	\bra{\Psi_1}F_2\ket{\Psi_1} &= 0,\\
	\bra{\Psi_2}F_2\ket{\Psi_2} &= \frac{c}{\sqrt{2}}.\\
\end{align*}


\section{Quantum game}
We now want to play a game of tic-tac-toe with our quantum computer. each of the 9 fields is one state. Since
a qubit can be in a superposition state, where it is in two states simultaneously, we can have a more complex
situation as in the classical system.

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\foreach \i in {0,2,4,6}{
			\draw[blue] (\i,0) -- (\i,6);
			\draw[blue] (0,\i) -- (6,\i);
		}
		\foreach \i in {1,3,5}{
			\draw (\i,0) -- (\i,6);
			\draw (0,\i) -- (6,\i);
		}
		
		\node[orange!60!black,anchor=center] at (0.5,5.5) {$x_1$};
		\node[orange,anchor=center] at (4.5,5.5) {$x_1$};
		\node[orange,anchor=center] at (0.5,4.5) {$x_2$};
		\node[orange,anchor=center] at (4.5,1.5) {$x_2$};
		\node[orange,anchor=center] at (5.5,5.5) {$0_1$};
		\node[orange,anchor=center] at (5.5,1.5) {$0_1$};
		\node[orange,anchor=center] at (4.5,3.5) {$0_2$};
		\node[orange,anchor=center] at (4.5,0.5) {$0_2$};
	\end{tikzpicture}
	\caption{Closed loop in a game of quantum tic tac toe.}
	\label{6.3 fig:tictactoe}
\end{figure}
\todo{Zeichne feld mit closed loop}
In the second round player two places his coins in field 3 and 9. Thus a closed loop is created 








