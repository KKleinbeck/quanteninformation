% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US

\chapter{Quantum Communication}
Classical communication relies on at least one transmitter and one receiver. The transmitter Bob has some information
input he wants to encode and sends to the receiver Alice, who wants to the decode the information. The communication
is along a classical channel, i.e., a string of bits is transmitted between the two players. So there can always be
an eves dropper Eve, who reads out the communication, makes a copy of it and send the copy to the receiver, without
her noticing that the message got read out before. This is why we are relying on proper encryption.

In the case of quantum information is different, there are different rules to play by. The most important is the
no cloning theorem. Once Eve read out the information she is not able to create a copy to send it to Alice. Alice
and Bob could also use a quantum channel, which allows entanglement between send qubits. We also learned about
quantum teleportation. Alice and Bob share a quantum channel and share an entangled state
\begin{equation*}
	\kPsi_{AB} = \left.\frac{1}{\sqrt{2}}\middle(\kz_A\ko_B + \ko_A\kz_B\right).
\end{equation*}
Alice further has the state $\kPhi$, which she wants to send to Bob. She entangles the state with $\kPsi_{AB}$ and
makes a Bell measurement on her qubits. She then announces her results on a classical channel. With this informations
Bob knows, which gate he has to apply to bring his qubit into the state $\kPhi$.

We can generalize this. If Alice and Bob share multiple entangled states, they can send each other many qubit strings,
without the danger of Eve getting to know their communication and by just using a classical channel (after the
entangled states got exchanged). There are other secure quantum key decryption protocols, like the BBS4 protocol,
which we previously studied. This is usually done with entangled photons. A photon is send on a non linear crystal,
where an entangled superposition of a horizontal and a vertical polarized photon is created
\begin{equation*}
	\Psi_{AB}= \left.\frac{1}{\sqrt{2}}\middle(\ket{H_1V_2} + \ket{V_1H_2}\right).
\end{equation*}
A mayor problem in this setup is the attenuation length $L$, the length after which the photon pair is decoherent.
Even for a good optical fiber this length is around \SI{20}{\kilo\meter}. Starting at a transfer rate of
up to $10^{12}$ bits per second this drops to $10^{-6}$ bits per second at a distance of just \SI{800}{\kilo\meter},
i.e., sending informations from south Germany to its furthest city in the north.

A quantum repeater is necessary to improve communication quality. The idea is to have many shorter segments at 
distance $L$, and the starting and ending point of neighboring segment are $l \ll L$ apart.
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\foreach \x in {0,1.4,2.8,4.2,5.6}
		{
			\draw[fill=white] (\x,0) circle (0.75mm) -- +(1,0) node[midway,anchor=north] {$L$} circle (0.75mm);
			\node[anchor=north] at (\x +1.2,0) {$l$};
%			\node[anchor=south] at (\x +1.2,0) {\node};
		}
	\end{tikzpicture}
	\caption{Sketch for segmentation of quantum repeaters.}
	\label{fig:sketch segments}
\end{figure}
At each of these node (ending to starting point connection) there is an entangled qubit pair, shared by the different
points. For example we sand the Bell pair $\kPsi_{AB}$ from point $A$ to $B$. At the segment $CD$ there is the
Bell pair $\kPsi_{CD}$. We entangle it with the incoming state $\kPsi_{AB}$ to get the total state
$\kPsi_{ABCD} = \kPsi_{AB}\otimes\kPsi_{CD}$. We perform a Bell measurement at the node $BC$. Like in quantum
teleportation the outcome of the measurement tells us, which gates we have to apply to the qubit at station $D$ to
get $\kPsi_{AD} = \kPsi_{AB}$. By this means we effectively moved the entangled state along two segments but without
increasing the decoherence.

Let's do a full example. We measure Qubit $B$ in the $x$ basis, we identify $\ko + \kz \to 0$ and $\ko - \ko \to 1$,
while we measure Qubit $C$ in the $z$ basis and map $\kz \to 0$ and $\ko \to 1$. The possible outcomes are listed in
table~\ref{tab:quantum repeater}. According to the result we will apply different gates to get to the wanted
state $\kz\ko + \ko\kz$. This procedure can be repeated multiple times at every node.
\begin{table}[!b]
	\centering
	\caption{Possible outcomes of the Bell measurement at one node of the quantum repeater.}
	\begin{tabular}{rcccc}\toprule
		B & 0 & 0 & 1 & 1\\
		C & 0 & 1 & 0 & 1\\\midrule
		$\kPsi_{AD}$ & $\kz\ko + \kz\kz$ & $\kz\kz + \ko\ko$ & $\kz\ko - \ko\kz$ & $\kz\kz - \ko\ko$ \\\bottomrule
	\end{tabular}
	\label{tab:quantum repeater}
\end{table}

Using a quantum repeater is very useful, the point at which we drop to a transfer rate of $10^{-6}$ bits per second
now comes just after a distance of 1500 to \SI{2000}{\kilo\meter} and the a much flatter decrease of the transfer
rate at the first few kilometers. To further increase the range of quantum communication we can do entanglement
purification. Imagine our initial state $\ket{\Psi^+} = \kzo + \koz$ can experience a bit flip error and get mapped
to $\ket{\Phi^+} = \kzz + \koo$. So the density matrix for this process is
\begin{equation*}
	\varrho = F\ket{\Psi^+}\bra{\Psi^+} + (1-F) \ket{\Phi^+}\bra{\Phi^+}.
\end{equation*}
To fix this possible error we will send $N$ entangled qubits at the same time. We want to extract at least one
perfect pair. For this we will entangle the first qubit of the first pair with the first qubit of the second pair
by means of a CNOT gate. We will do the same thing for the second qubits of the two pairs. After this we measure the
qubits of the second pair. Under this procedure the density matrix for the for qubits becomes
\begin{equation*}
	\varrho = \varrho_{12}\otimes\varrho_{34}
		= F^2\ket{\Psi^+}_{12}\bra{\Psi^+}_{12}\ket{\Psi^+}_{34}\bra{\Psi^+}_{34}
		+ (1-F)^2\ket{\Phi^+}_{12}\bra{\Phi^+}_{12}\ket{\Phi^+}_{34}\bra{\Phi^+}_{34} + \dots.
\end{equation*}
We want to arrive at $\varrho = \kPsi\bPsi$, so let us look at one of the possible products
\begin{align*}
	\kPsi_{12}\kPsi_{34} &= \big(\kz_1\ko_2 + \ko_1\kz_2\big)\otimes\big(\kz_3\ko_4 + \ko_4\kz_3\big)\\
	&\overset{\mathrm{CNOT}}{\longrightarrow}
		\big(\kz_1\ko_2 + \ko_1\kz_2\big)\otimes\big(\kz_3\ko_4 + \ko_4\kz_3\big)\\
	&\qquad + \big(\ko_1\ko_2 + \ko_1\ko_2\big)\otimes\big(\kz_3\kz_4 + \ko_4\ko_3\big).
\end{align*}
So, if the measurement of the third and forth qubits yield the same result we know that we are now in the 
$\ket{\Phi^+}_{12}$ state, in the other case, where one qubit is in the state $\kz$ and one in the state $\ko$ we are
still in the correct state $\ket{\Psi^+}_{12}$. The same goes for the possible $\ket{\Phi^+}_{12}\ket{\Phi^+}_{34}$
product, where we can guarantee to end in $\ket{\Phi^+}_{12}$ after sacrificing qubits $3$ and $4$.
so the new density matrix, after the first node and sacrificing becomes
\begin{equation*}
	\varrho = F' \ket{\Psi^+}_{12}\ket{\Psi^+}_{12} + (1-F')\ket{\Phi^+}_{12}\ket{\Phi^+}_{12},
\end{equation*}
where
\begin{equation*}
	F' = \frac{F^2}{F^2 + (1 - F)^2}.
\end{equation*}
Clearly we want $F'$ to be bigger than $F$. We can repeat this procedure, so getting to a $F''$ and so on. For many
pairs we can bring the fidelity up to $F^{(N)} = 1$.






































