% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US

\chapter{Stabilizer formalism}
Our goal is to construct codes that can encode quantum information. We should be able to detect and correct
errors, as well to perform operations on the encoded subspace. For example we have a look at to subsets of
operators
\begin{align*}
	S &= \{S_1,S_2,\dots,S_r\}\\
	T &= \{T_1,T_2,\dots,T_m\}.
\end{align*}
Supposed those subsets anticommutes, i.e., $\{T_i,S_j\} = 0$. Now, if $\kPsi$ is an eigenstate to the operator
$S$ with the eigenvalue $\lambda$, than $T\kPsi$ is eigenvector to $S$ with eigenvalue $-\lambda$. Thus there
is basis, in which we can diagonalize both $S$ and $T$.

For example we can build $S$ and $T$ with Pauli-operators, like
\begin{align*}
	S &= Z \otimes Z \otimes \mathbb{1}\\
	T &= X \otimes \mathbb{1} \otimes \mathbb{1}.
\end{align*}
For now, we will again work in the three qubit basis, as we did in the previous chapter for our error
correction strategies. One can easily verify, that those $S$ and $T$ fulfill the anticommutator relation.

\section{Excursion: Group Theory}
\todo{Gruppenaxiome}

With this knowledge we find that the Pauli-operators $X$, $Y$ and $Z$, together with the identity $\mathbb{1}$,
builds a non-abelian group, the pauli group $P$. For a $n$-qubit subspace we define the $n$-pauli group
\begin{equation*}
	\mathcal{P} = \{P_1\otimes P_2 \otimes \dots\otimes P_n\}.
\end{equation*}
One can check that this definition indeed results in a group.

\section{Stabilizer Group}
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\filldraw[blue,fill=blue!20!white] (0,0) circle (2.5);
		\node[blue] at (2,2) {$P_n$};
		
		\filldraw[orange,fill=orange!50!white] (-0.5,-0.5) circle (0.8);
		\node[orange] at (-1,0.4) {$S$};
		
		\filldraw[green!50!black,fill=green!80!black] (0.75,0.75) circle (0.75);
		\node[green!50!black] at (1.75,0.5) {$E$};
	\end{tikzpicture}
	\caption{Out of the set of all pauli operatores $P_n$ there is the subset of the stabilizer group $S$
		and the error group $E$, with $ES= -SE$. Only errors out of this subspace can be detected and
		corrected.}
\end{figure}
We take a subspace of the pauli group, the stabilizer group $S \subset \mathcal{P}$. We demand the properties
\begin{enumerate}
	\item For every pair of operators $S_i,S_j \in S$ the commutator $[S_i,S_j] = 0$ vanishes.
		This basically reads that $S$ is an abelian group.
	
	\item The group $S$ does \emph{not} contain $-\mathbb{1}$.
\end{enumerate}
Typically one does not want to write down all elements, when defining a group. Thus one can write down the
generators; from which every other element can be build. For example the group
\begin{equation*}
	S = \{Z\otimes Z\otimes\mathbb{1},\mathbb{1}\otimes Z\otimes Z, Z \otimes \mathbb{1} \otimes Z,
		\mathbb{1},\mathbb{1},\mathbb{1}\},
\end{equation*}
can be build by the generator
\begin{equation*}
	G = \{Z\otimes Z\otimes \mathbb{1},Z\otimes\mathbb{1}\otimes Z\}.
\end{equation*}

Now we want to find the code space for the stabilizers $\{S_1,\dots,S_r\}$, with $S_i^2 = \mathbb{1}$. This
last property tells us, that only eigenvalues $\pm 1$ are possible. One also finds $\operatorname{Tr}S_i = 0$,
as long $S_i \ne \mathbb{1}$, since the pauli operators have a vanishing trace. Since the trace is the sum
of all eigenvalues and the $S_i$ are $2^n$ dimensional matrices, we know, that there are $2^{n-1}$ eigenvectors
with eigenvalue $+1$ and $2^{n-1}$ eigenvectors with eigenvalue $-1$.

According to group theory, we can define the projection operators
\begin{align*}
	P_i^+ &\equiv \frac{1}{2}(\mathbb{1} + S_i),\\
	P_i^- &\equiv \frac{1}{2}(\mathbb{1} - S_i),
\end{align*}
where $P^+$ projects onto the $+1$ eigenspace and $P^-$ on the $-1$ eigenspace. Now we look at the product
$P_1^+ S_2$. Since $S_1$ and $S_2$ have the same eigenvectors and $P^+$ projects on the positive subspace, we
directly can compute the outcome of $P_1^+ S_2 \kPsi = +1 \kPsi$. Again, $\operatorname{Tr}P_1^+ S_2 = 0$,
so the eigenvectors again split in a positive and a negative subspace. But since we apply $S^+$ we are
already in a $2^{n-1}$ dimensional subspace, so, after applying $P_1^+ S_2$ we end up in a $2^{n-2}$
dimensional subspace. This positive subspace is our computational space.

In this subspace we know that
\begin{equation*}
	\bra{\Psi_j} S \ket{\Psi_i} = \delta_{ij},
\end{equation*}
which will be useful in further calculations. We now take any element $P$ of the Pauli group.
We now calculate
\begin{align*}
	\ket{\Psi_j}P\ket{\Psi_i} &= \ket{\Psi_j}PS\ket{\Psi_i}\\
	&= -\ket{\Psi_j}SP\ket{\Psi_i}\\
	&= -\ket{\Psi_j}P\ket{\Psi_i}.
\end{align*}
This results in $\ket{\Psi_j}P\ket{\Psi_i} = 0$, our criteria we will use for error correction of so called
"`pauli erros"', i.e., errors $\{E_a\}$ which can be expressed as pauli matrices. Such errors will be corrected
in this formalism, e.g., $E = \{X\otimes\mathbb{1}\otimes\mathbb{1},\mathbb{1}\otimes X\otimes\mathbb{1},
\mathbb{1}\otimes\mathbb{1}\otimes X\}$.

\section{Gates on Logical Qubits}
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\draw (0,0) node[anchor=east] {$\kz$} -- (2,0) node[draw,fill=white] {$H$} -- (6,0) 
			node[draw,fill=white] {$H$} -- (8,0) node[anchor=west] {measure};
		\draw (4,2) -- (4,0) circle (2.5pt);
		\draw (0,2) node[anchor=east] {$\kPsi$} -- (4,2) node[draw,fill=white] {$E$} -- (8,2)
			node[anchor=west] {$\pm\kPsi$};
	\end{tikzpicture}
	\caption{Error detection mechanism.}
	\label{fig:stabilizer}
\end{figure}
We used logical qubits of the form
\begin{align*}
	\kz_L &= \kzzz\\
	\ko_L &= \kooo,
\end{align*}
to perform error corrections. So how does one apply gates on logical states $\alpha \kz_L + \beta \ko_L$?
Operations should not take you outside of this logical subspace '$S$'. This gives the condition $[P,S] = 0$,
for any operator $P$. This leads to $S\subseteq P$.

We introduce the normalizer subspace $N = P \setminus S$. Keep in mind, since $N$ is a subset of $P$, every
operator out of $N$ commutes with $S$. IF you have $n$ qubits and $r$ generators of $S$, than you need
$r$ ancillary qubits to detect the error. These $r$ qubits will be measured and depending of the outcome
correct the original $n$ qubits. Figure \ref{fig:stabilizer} shows a sketch of this procedure. The error $E$
changes all the $n+r$ qubits. Depending on the measurement of the $\kz$ ancillary qubit one has to perform
corrections or not.








% Outlook
%\chapter{Toxic Code}
%\chapter{Superconducting Qubits}
%\section{Theory}
%\section{Experiment}
%\chapter{Cavity QED based Quantum Computing}
%\section{Theory}
%\section{Experiment}
%\chapter{One-way Quantum Computing}
%\chapter{Quantum Internet}
