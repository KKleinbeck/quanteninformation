% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US

To proof this, we can go back to the taylor representation of the Operator $U(t)$
\eqref{4.2 eq:time develop series}. We then see
\begin{align*}
	U(t) &= 1 + (-ix) + \frac{1}{2} (-ix)^2 + \frac{1}{6} (-ix)^3 + \dots \\
	&= 1 - x^2 + \frac{x^2}{4!} - \frac{x^6}{6!} + \dots \\
	&\phantom{=} ix - i\frac{x^3}{3!} + i\frac{x^5}{5!} + \dots \\
	&= \cos(x) + i \sin(x),
\end{align*}
where we used $x = Ht/ \hbar$. 

Let's look at an example. We have a spin system with the states $\kup$ and $\kdown$ and we prepare a quantum
bit in the initial state
\begin{equation*}
	\ket{\Psi(t = 0)} = \kup.
\end{equation*}
We use the $U_x(t)$ gate to rotate the quantum bit
\begin{equation*}
	\kPsit = \cos\left(\frac{B_xt}{2}\right)\kup + i \sin\left(\frac{B_xt}{2}\right)\kdown.
\end{equation*}
This will result in an NOT gate, if we choose $B_xt = \pi$, since we have the final state
\begin{equation*}
	\kPsit = i \kdown.
\end{equation*}

\subsection*{Hadamared Gate}
We look back at the Hadamard gate, which we defined as
\begin{equation*}
	H = \frac{1}{\sqrt{2}}
	\left(
	\begin{matrix}
		1 & 1 \\ 1 & -1
	\end{matrix}
	\right).
\end{equation*}
With everything we now know, we can build this gate by using $H = \frac{2}{\hbar}\left(S_z + S_x \right)$. But
we can simplify this further, when we consider the operator
\begin{equation*}
	\left(
	\begin{matrix}
		1 & i \\ i & 1
	\end{matrix}
	\right),
\end{equation*}
which is equivalent to an rotation $U_x(t)$ with an angle $B_x(t) = \pi/2$. This operator is equal to the
Hadamard gate (without a constant phase).



\chapter{Measurements}
We have an observable $A$ and a system in state $\kPsi$. $A$ has the eigenvalues $a_i$ with eigenstate
$\ket{a_i}$. The probability to get $a_i$ after one measurement is
\begin{equation*}
	|\braket{a_i}{\Psi}|^2 = P_i.
\end{equation*}
After the measurement the system system is then in the eigenstate $\ket{a_i}$. Each observable must be
achieved by the ensemble average
\begin{equation*}
	\langle A\rangle = \bra{\Psi} A \ket{\Psi}.
\end{equation*}
The standard deviation is
\begin{equation}
	\langle (A - \langle A \rangle)^2 \rangle \ge 0,
\end{equation}
with equal zero, only if $\kPsi$ is an eigenstate of $A$.


\chapter{Deutsch-Josza Algorithm}
We define a function as
\begin{equation}
	f: \{0,1\} \rightarrow \{0,1\}, \qquad x \mapsto f(x),
\end{equation}
and we ask us-self the question ,whether $f$ is constant or balanced. The Deutsch-Josza algorithm is a fast
and clever algorithm, which can answer this quastion after just one measurement! First of all, we list all
codings of $f$:
\begin{align*}
	f_1: & 0 \mapsto 0, 1 \mapsto 1, \\
	f_2: & 0 \mapsto 1, 1 \mapsto 0, \\
	f_3: & 0 \mapsto 0, 1 \mapsto 0, \\
	f_4: & 0 \mapsto 1, 1 \mapsto 1.
\end{align*}
We want to write the $f_i$ as $2\times 2$ matrices, which results in
\begin{align*}
	f_1 &= \mathbb{1}, \\
	f_2 &= X, \\
	f_3 &= P_+ + \frac{1}{\hbar} S_+,\\
	f_4 &= P_- + \frac{1}{\hbar} S_-,
\end{align*}
where $X$ is the according Pauli matrix, $P_i$ are projection operators and $S_i$ spin operators. Let's have
for example a closer look at $f_3$
\begin{align*}
	P_+ + \frac{1}{\hbar} S_+ &= 
	\left(
	\begin{matrix}
		1 & 0 \\ 0 & 0
	\end{matrix}
	\right)
	+
	\left(
	\begin{matrix}
		0 & 1 \\ 0 & 0
	\end{matrix}
	\right) \\
%
	&=
	\left(
	\begin{matrix}
		1 & 1 \\ 0 & 0
	\end{matrix}
	\right),
\end{align*}
which obviously yields in the correct operator $f_3$.

The algorithm needs 2 qubits (NV quantum computer: 1 electron and 1 nucleon spin). $U_f(x,y)$ is the
according unitary operator, which acts as
\begin{equation*}
	U_f\ket{x,y} = \ket{x, y\otimes f(x)},
\end{equation*}
where $\otimes$ is addition modulo 2, which we can build by suing XOR or CNOT gates. We look at the different
input states $\kzz$, $\kzo$, $\koz$ and $\koo$ and how $U_f$ changes them. The outputs are in the case
$f = f_1$
\begin{table}[!t]
	\centering
	\caption{CNOT gate as truth table}
	\begin{tabular}{ccc}\toprule
		$x$ & $y$ & CNOT \\\midrule
		0 & 0 & 0 \\
		0 & 1 & 1\\
		1 & 0 & 1\\
		1 & 1 & 0\\\bottomrule
	\end{tabular}
\end{table}
\begin{align*}
	\kzz &\mapsto \kzz, \\
	\kzo &\mapsto \kzo, \\
	\koz &\mapsto \koo, \\
	\koo &\mapsto \koz.
\end{align*}
Since our output can be all of the 4 possible states the function is obviously balanced. We see and remember,
this gate flips the second qubit, if the first one is in the state~$\ko$.

Now we come to the \emph{compiler} step: the execution of the Deutsch-Josza algorithm by using known gates.
\begin{align*}
	U_f \ket{x,0} &= \ket{x,0\otimes f(x)}\\
	U_f \ket{x,1} &= \ket{x,1\otimes f(x)}
\end{align*}
The strategy is as follows: we take any first qubit state $\ket{x}$ and a fixed second qubit state, for example
$\kz$. We apply the Hadamard gate (which we already showed how we can build it) to the $x$-qubit
\begin{align*}
	H_x \ket{0} &= \frac{1}{\sqrt{2}} \left( \kz + \ko \right)\\
	H_x \ket{1} &= \frac{1}{\sqrt{2}} \left( \kz - \ko \right)
\end{align*}

If we then apply the operator $U_f$ which computes $f$, we get
\begin{equation*}
	U_f H_x \kzz = \frac{1}{\sqrt{2}} U_f \left(\kzz + \kzo \right)
		= \frac{1}{\sqrt{2}} \left(\ket{0,f(0)} + \ket{1,f(1)} \right).
\end{equation*}
We now use the Hadamard gate on a given initial state $\kzo$, which results in the state
\begin{equation*}
	\ket{\Psi_1} H_x H_y \kzo = \frac{1}{2}\left(\kzz - \kzo + \koz - \koo \right).
\end{equation*}
We then apply $U_f$ and we get the state
\begin{equation*}
	\ket{\Psi_2} = U_f \ket{\Psi_1}
		= \frac{1}{2}\Big(\ket{0,0\otimes f(0)} - \ket{1,0\otimes f(1)}
		+ \ket{0,1\otimes f(0)} - \ket{1,1\otimes f(1)} \Big).
\end{equation*}
Consider now a function $f$, which results always in the same output $f(0) = f(1)$. This simplifies the
state $\ket{\Psi_2}$ to
\begin{align}
	\notag
	\ket{\Psi_2} &= \frac{1}{2}\Big(\ket{0,f(0)} - \ket{1,f(0)}
		+ \ket{0,1\otimes f(0)} - \ket{1,1\otimes f(0)} \Big). \\
	&= \frac{1}{2} \underset{\ket{x_1}}{\underbrace{\Big(\kz + \ko \Big)}}
		\underset{\ket{y}}{\underbrace{\Big(\ket{f(0)} - \ket{1\otimes f(0)} \Big)}}.
	\label{6.1 eq:result 1}
\end{align}
The second possible realization of the function $f$ has the property $f(1) = 1\otimes f(0) \ne f(0)$, which
yields in the result
\begin{align}
	\notag
	\ket{\Psi_2} &= \frac{1}{2} \Big(\ket{0,f(0)} + \ket{1,1\times f(0)}
		- \ket{0, 1\otimes f(0)} - \ket{1,1\otimes 1 \otimes f(0)}\Big) \\
	&= \frac{1}{2}\underset{\ket{x_2}}{\underbrace{\Big(\kz - \ko \Big)}}
		\Big( \ket{f(0)} - \ket{1\otimes f(0)} \Big).
	\label{6.1 eq:result 2}
\end{align}
We compare the $\ket{x_2}$ state of this result with the $\ket{x_1}$ state \eqref{6.1 eq:result 1} from the
first case, we see, that $\ket{x_1}$ and $\ket{x_2}$ are orthogonal. We have transformed the properties of
the function $f$ into properties of the output state. One measurement of the output state will immediately
gives us the answer, whether $f$ is constant or balanced.
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
			\draw (0,0) node[anchor=east] {$y$} -- (1,0) node[draw=black,fill=white] {$H$} -- (2,0);
			\draw (0,1) node[anchor=east] {$x$} -- (1,1) node[draw=black,fill=white] {$H$} -- (2,1);
			\draw (2,-0.2) -- (2,1.2) -- (3,1.2) -- (3,-0.2) -- (2,-0.2);
			\node at (2.5,0.5) {$U_f$};
			\draw (3,0) -- (5,0);
			\draw (3,1) -- (4,1) node[draw=black,fill=white] {$H$}
				-- (5,1) node[anchor=west,fill=white] {measurement};
	\end{tikzpicture}
	\caption{Wire Diagram of the Deutsch-Josza algorithm.}
\end{figure}
