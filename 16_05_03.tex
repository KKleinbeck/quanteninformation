% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US

\chapter{Implementations of Quantum Computation}

We already talked about the DiVincenzo criteria, for when a system is a working quantum computer.
\begin{itemize}
	\item the system needs to be scalable.
	
	\item You need to initialize it.
	
	\item You need to be able to do controlled manipulations.
	
	\item The qubits need have long coherence times.
	
	\item You must be able to perform a optimal readout.
\end{itemize}
In the last years to further criteria were added.
\begin{itemize}
	\item You need good coupling of your flying qubits (photons).
	
	\item You need to be able to exchange information with photons.
\end{itemize}

In nature atoms normally have a non linear level system, ideal for qubits, since you can control your
subspace of states very well. Working with a scalable system of atoms is tough though. So one wants to
recreate atom-like behavior with classical circuits, i.e., create artificial atoms. We already looked at the
$L$-$C$ circuit, as a quantizable system, where the energy was given by
\begin{equation*}
	H = \frac{Q^2}{2C} + \frac{\Phi^2}{2L}.
\end{equation*}
We see the equation for a harmonic oscillator, where the charge $Q$ and the magnetic flux $\Phi$ are the
canonical variables. So the energy levels are given by $E_n = \hbar \omega (n + \frac{1}{2})$, with the
frequency $\omega = \sqrt{LC}^{-1}$.

This system is definitely scalable, as long as the $L$-$C$ circuits are so far away, that they do not couple.
By cooling the system below $k_\mathrm{B}T= \hbar\omega$, which is typpically at the order of
$T\approx\SI{0.5}{\kelvin}$, we are assured to be in the ground state. Experimentally operating temperatures
are at \SI{10}{\milli\kelvin}. This cooling procedure is the initialization step of our criteria. Next we
have to look at manipulations on the qubits.

Let's look at the classical variables first. The magnetic flux was given by
\begin{equation*}
	\varphi(t) = \int_{-\infty}^{t} V(t')\dd t.
\end{equation*}
In the quantum mechanical picture it is proportional to the ladder operator $a^\dagger$, so varying the
voltage allows us to project from the ground state in the first excited state and backwards. This means we
can perform operations on our system by applying microwave sources for example.

Coherence in this system is very bad, due to damping from resistance in the system
\begin{equation*}
	\frac{\dd^2 I}{\dd t^2} + \frac{1}{LC} I + \gamma \dot{I} = 0.
\end{equation*}
Readout can be performed by a second $L$-$C$ circuit. We will bring it close to the first one and measure the
induction voltage, since the magnetic flux in the first system will influence the second one. We will call
that a dispersive readout.

We are left with two major problems. The first concerns the energy levels. They are non linear, so one is not
guaranteed to stay in the computational subspace. The other problem is the mentioned dissipation.

\section{Superconducting Qubits}
One idea to fix those problems is by replacing the inductor by a Josephon junction. A Josephon junction is a
junction between two supra conductor with a isolator in the middle. The current at the junction is given by
\begin{equation*}
	I = I_0 \sin\delta,
\end{equation*}
where $\delta = \vartheta_1-\vartheta_2$ is the phase difference of the two cooper pair wave functions in the
supra conductors. The change in phase depends on the voltage at the junction
\begin{equation*}
	\frac{\dd \delta}{\dd t} = \frac{2\pi}{\Phi} V.
\end{equation*}
Compare now the quantum mechanical result
\begin{equation*}
	V = \frac{\Phi}{2\pi I_0\cos\delta}\dot{I}
\end{equation*}
with the classical result in the $L$-$C$ circuit
\begin{equation*}
	V = - L\dot{I}.
\end{equation*}
So we define a Josephon inductivity
\begin{equation*}
	L_J = \frac{L_{J_0}}{\cos\delta}.
\end{equation*}
The Josephon inductivity is a function of the magnetic flux.

The Josephon energy is similarly given by
\begin{equation*}
	E_J = \int V I \dd t = \frac{\Phi I_0}{2\pi}\cos\delta.
\end{equation*}
Now the Hamiltonian is given by
\begin{equation*}
	H = \frac{Q^2}{2C} - E_{J_0}\cos\left(2\pi \frac{\Phi}{\Phi}\right),
\end{equation*}
where we have used the expression $\delta = 2\pi \Phi/\Phi_0$. $\Phi_0$ is the quantum flux.

$H$ gives you the Mathieu functions as solutions; a expansion in second order yields in an harmonic oscillator
again. Since we are dealing with cooper pairs, the quantized charge reads $\hat{Q} = 2e\hat{n}$ with $n$ the
number of cooper pairs and the Hamilton operator becomes
\begin{equation*}
	H = E_C \hat{n}^2 - E_{J_0}\cos\left(2\pi\frac{\hat{\Phi}}{\Phi_0}\right).
\end{equation*}

Of course we want to control both terms in the Hamiltonian. The number of cooper pairs can be controlled be
adding another capacitor between the Jospehon-$C$ circuit and the source. The number of cooper pairs that are
fixed at the new capacitor can be varied through varying the voltage and the conducting energy reads
\begin{equation*}
	E_C(\hat{n} - n_J)^2.
\end{equation*}
The Flux can be manipulated by replacing the Josephon junction by a SQUID. Then the current becomes
\begin{equation*}
	I_J \approx I_0\cos\left(\frac{\Phi_{enc}}{\Phi_0}\right)
		\sin\left(\delta + 2\pi\frac{\Phi_{enc}}{\Phi_0}\right),
\end{equation*}
where $\Phi_{enc}$ is the enclosed magnetic flux by the SQUID.

Now there are many possible qubits, like charge qubits (cooper pairs), flux qubits, phase qubits, transmon qubits
or fluxmon qubits. This basically means, that charge, flux and phase are good quantum numbers.
\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\draw (0,0) rectangle (1,1.5);
		\draw (0,0) -- (1,1.5) -- (0,1.5) -- (1,0);
		\node at (1.5,0.75) {=};
		
		\draw (3,0.8) -- (3,1.5) -- (2,1.5) -- (2,0) -- (3,0) -- (3,0.7);
		\draw (2.75,0.7) -- (3.25,0.7);
		\draw (2.75,0.8) -- (3.25,0.8);
		
		\draw (1.8,0.55) -- (2.2,0.95);
		\draw (1.8,0.95) -- (2.2,0.55);
	\end{tikzpicture}
	\caption{Short notation for the Josephon-$C$ circuit.}
\end{figure}
\todo{draw additional figures for charge qubit}

The charge on the island is $n_g = \frac{Q}{2e}$ and the total capacity is $c_T = c_g + c_J$.
In this setup the Hamiltonian reads
\begin{equation*}
	H = \frac{(2e)^2}{c_g + c_J} (\hat{n} - n_g)^2 - E_J\cos\Phi.
\end{equation*}

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[width=0.5\textwidth,
					axis lines=middle,
					xtick={3,5,7},
 %					xtick label={-1,$n_g$,+1},
					xmin=0,xmax=9,]
			\addplot[domain=0:9,] {(x-5)^2};
			\addplot[domain=0:9,dashed,blue] {(x-3)^2};
			\addplot[domain=0:9,dashed,orange] {(x-7)^2};
			
			\node at (axis cs:5,40) {$E_J = 0$};
			\node[blue] at (axis cs:5,35) {$E_J \ne 0$};
		\end{axis}
	\end{tikzpicture} %
	\begin{tikzpicture}
		\begin{axis}[width=0.5\textwidth,
					axis lines=middle,
					xmin=0,xmax=10]
			\addplot[domain=0:10,samples=40,blue] {-sin(deg(x)) + 3.5};
			\addplot[domain=0:10,samples=40,orange] {sin(deg(x)) + 1};
		\end{axis}
	\end{tikzpicture}
	\caption{Band-like structures emerging from Josephon-$C$ circuit with charge qubits.}
\end{figure}

For the $n=0$ and $n=1$ subspace the Hamiltonian can be approximated by
\begin{equation*}
	H = \tilde{E}_C\sigma_z + \tilde{E}_J\sigma_x.
\end{equation*}
