% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US


\chapter{Toric code: Self correcting systems}
In 2003 A.~Kitaev showed, that there are some systems, which itself can perform some form of error corrections.
We can take advantage of some lattice, which have a huge energy gap between the ground and the first excited
state and have certain symmetries. The first property makes errors unlikely, the latter gives us directly the
stabilizer group of our code space.

Remember: we want to protect and operate on our logical qubits. The stabilizer group spans the code space and
give as the ability for an error syndrome measurement.  We introduced the normalizer group. which commutes
with the stabilizer, but don't lie in them. The Error group was split into a correctable and not correctable
subgroup. We need to do the same things for these lattices.

We start by creating a $L\times L$ lattice with periodic boundary conditions, as shown in
figure~\ref{fig:Kitaev lattice}.
\begin{figure}[!b]
	\centering
	\begin{tikzpicture}
		\foreach \x in {0,...,5}
		{
			\draw (\x,0) -- (\x,5);
		}
		
		\foreach \y in {0,...,5}
		{
			\draw (0,\y) -- (5,\y);
		}
		
		
		\begin{scope}
			\clip (0,0) rectangle (5,5);
			\foreach \x in {0,...,5}
			{
				\foreach \y in {0,...,5}
				{
					\fill[blue!50!white] (\x + 0.5,\y) circle (2pt);
					\fill[blue!50!white] (\x,\y+0.5) circle (2pt);
				}
			}		
		\end{scope}
		
		\foreach \x/\y in {1.5/2,2.5/3,3.5/3,4.5/2}
		{
				\node[anchor=south,inner sep=3pt] at (\x,\y) {\small $X$};
		}
		
		\draw[red] (2,1) -- (2,3);
		\draw[red] (1,2) -- (3,2);
		\node[red,anchor=north west] at (2,2) {v};
		
		\draw[green!50!black] (3,3) rectangle (4,4);
		\node[green!50!black,anchor=north west] at (3,3) {p};
	\end{tikzpicture}
	\caption{Qubits (blue circles) on a lattice. Half qubits on the boundary are continued on the
		other side of the lattice. A vertex (red) and plaquette (green) is marked. A $X$-error chain,
		is marked on the qubits.}
	\label{fig:Kitaev lattice}
\end{figure}
The stabilizers are the vertices (marked in red)
\begin{equation*}
	A_V = X \otimes X \otimes X \otimes X
\end{equation*}
and the plaquettes (squars in green)
\begin{equation*}
	B_P = Z\otimes Z \otimes Z\otimes Z.
\end{equation*}

It's trivial to show that the vertices and plaquettes commute: If the have no qubits in common they act on
different subspaces and commute, in every other case they have exactly two qubits in common and we calculate
\begin{equation*}
	[X\otimes X, Z\otimes Z] = 0.
\end{equation*}
So we have $[A_V,B_P] = 0$ for every plaquette $P$ and every vertex $V$. In total we have $L^2$ vertex
operators and $L^2$ plaquette operators. It is also easy to see, that
\begin{equation}
	\prod_{V} A_V = \prod_{P} B_P = \mathbb{1},
\end{equation}
since every given vertex (plaquette) overlaps with exactly 4 different vertices (plaquettes) on one different
qubit qach. For every lattice site $i$ we have the term $X_i X_i = \mathbb{1}$ ($Z_iZ_i=\mathbb{1}$) just
once. This gives us two constraints, which means that we have $2L^2 - 2$ generators of the stabilizer group.

So what is the code space in this example? How many logical qubits are there? All stabilizer fulfill
$S_i \kPsi = \kPsi$, which gives us a condition on the code space. The $L\times L$ has $2L^2$ qubits in total.
Subtracting the $2L^2 - 2$ stabilizers we are left with $2$ logical qubits. The lattice can encode two logical
qubits.

Next, we have to find the possible errors, which can occur in such a system. Of course
there are single qubit errors that are independent with respect to one another. For example measuring
\begin{equation*}
	A_{V_1} \kPsi = - \kPsi
\end{equation*}
and
\begin{equation*}
	A_{V_2} \kPsi = - \kPsi
\end{equation*}
gives us an error syndrome for a $Z$ error on the qubit connecting the vertices $1$ and $2$. A $X$-error
would be detected by
\begin{align*}
	B_{P_1} \kPsi &= -\kPsi\\
	B_{P_2} \kPsi &= -\kPsi.
\end{align*}

But, since there are so many qubits in this system, there could be an \emph{error chain}. Such an chain is
for $X$-errors is marked in figure~\ref{fig:Kitaev lattice}. Measuring the plaquettes or vertices along such a
chain, one would only find a syndrome result at the end of the chain. But we cannot measure the exact path of
the error chain.

\begin{figure}[!]
	\centering
	\begin{tikzpicture}
		\draw plot [smooth cycle] coordinates {(0,0) (1,1) (3,1) (2,-1)};
		\begin{scope}
			\clip plot [smooth cycle] coordinates {(0,0) (1,1) (3,1) (2,-1)};
			\node at (1,0) (L1) {};
			\node at (2,0.5) (L2) {};
			
			\fill[black] (L1) circle (1.5pt);
			\fill[black] (L2) circle (1.5pt);
			
			\draw plot[smooth] coordinates {(L1) (1.5,-0.2) (L2)};
			\node[anchor=north] at (1.5,-0.2) {$c_1$};

			\draw[dashed] plot[smooth] coordinates {(L1) (1.7,0.7) (L2)};
			\node[anchor=south] at (1.7,0.7) {$c_2$};
			
			\draw[dotted] plot[smooth] coordinates {(L1) (0.5,-0.5) (-2,-1)};
			\draw[dotted] plot[smooth] coordinates {(L2) (2.3,0.7) (3,1.5)};
			\node[anchor=north west] at (2.3,0.7) {$c_3$};
		\end{scope}
	\end{tikzpicture}
	\caption{Different paths to close error chains.}
\end{figure}

\begin{figure}[!b]
	\centering
	\begin{tikzpicture}
		\foreach \x in {0,...,5}
		{
			\draw (\x,0) -- (\x,5);
		}
		
		\foreach \y in {0,...,5}
		{
			\draw (0,\y) -- (5,\y);
		}
		
		
		\begin{scope}
			\clip (0,0) rectangle (5,5);
			\foreach \x in {0,...,5}
			{
				\foreach \y in {0,...,5}
				{
					\fill[blue!50!white] (\x + 0.5,\y) circle (2pt);
					\fill[blue!50!white] (\x,\y+0.5) circle (2pt);
				}
			}		
		\end{scope}
		
%		\foreach \x/\y in {1.5/2,2/2.5,3/2.5,3.5/2,3/1.5,2/1.5}
%		{
%				\node[anchor=south east,inner sep=3pt] at (\x,\y) {\small $X$};
%		}
		
		\draw[gray] plot[smooth cycle] coordinates {(1.5,2) (2,2.5) (3,2.5) (3.5,2) (3,1.5) (2,1.5)};
		\node at (3.5,1.5) {$N_I$};
		
		\draw[gray] (0.5,0) -- (0.5,5) node[black,anchor=south west] {$O_I$};
	\end{tikzpicture}
	\caption{Normalizer $N_I$ on the lattice. Note that $O_I$ doesn't belong to the normalizer group, since
		it's closed from the outside.}
	\label{fig:kitaev normalizer}
\end{figure}

If the error happens in a homological trivial, i.e., can be closed from the inside ($c_1 + c_2$), the error
is correctable. If the path has to be closed from outside ($c_1 + c_3$), we call that homological non trivial,
the error is not correctable.

Another kind of chain errors are closed loops, which on the one hand are not detectable, but on the other hand
have no effect on the code. We can see now, how we can correct the chain errors: we just have to introduce
additional error, such that the error path gets closed from the inside. This way, we also see, why we cannot
correct errors of chains, that have to get closed from the outside. Closing the paths can be done by finding
the minimum weight of the path.

The last thing we have to do is to find the normalizer group. They are given by every closed error path
\emph{inside} the lattice
\begin{equation*}
	N_I = X_1\dots X_6.
\end{equation*}
It trivially commutes with $A_V$ and $B_P$, as we already discussed. For the paths closed from outside, like
\begin{equation*}
	O_I = Z_1 \dots Z_L,
\end{equation*}
sketched in figure~\ref{fig:kitaev normalizer}, this does not hold. $O_I$ overlaps with the vertices on only
one qubit, which results in $[O_I,A_V] \ne 0$.















