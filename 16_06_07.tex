% !TeX root = Quanteninfo.tex
% !TeX spellcheck = en_US

\section{Adiabatic theorem}
For a first example we will look at the single qubit Ising Hamiltonian
\begin{equation*}
	H(t) = \frac{t}{T} \sigma_X + \left(1- \frac{t}{T}\right)\sigma_z
\end{equation*}
At the start the Hamiltonian reads $H(0) = \sigma_z$, so it has the eigenvalues $\pm1$ with the eigenstates
$\kup$ and $\kdown$. After a time $t=T$ we end in $H(t=T) = \sigma_x$, where the eigenvalues are still $\pm 1$
but the eigenstates became $\kup \pm \kdown$. If this evolution process is slow, the probability of a transition
between the different eigenvalues decreases; only for a adiabatically process the probability for a transition from
the ground state to the $E = 1$ states vanishes. We want to find the transition probability for the time parameter
$T$.

The process also depends on the eigenvalue spectrum under the evolution. Figure~\ref{fig:eigenvalues pert}
sketches this idea. Under the time evolution the two eigenvalues may come closer together, even overlap. The closer
the eigenvalues get the more likely a transition becomes.

\begin{figure}[!t]
	\centering
	\begin{tikzpicture}
		\begin{axis}[ylabel=$E$,
					ytick={0,1},
					xlabel=$t$,
					xtick={0,1},
					xticklabels={0,$T$},]
			\addplot[domain=0:1,blue] {(x-0.5)^2 + 0.75};
			\addplot[domain=0:1,orange] {-(x-0.5)^2 + 0.25};
			\draw[{latex}-{latex},thick]
				(axis cs:0.5,0.25) -- (axis cs:0.5,0.75) node[midway,anchor=west] {$\Delta E$};
		\end{axis}
	\end{tikzpicture}
	\caption{The eigenvalue of the systems change under perturbation and an initial gap can decrease and even
		vanish under perturbation.}
	\label{fig:eigenvalues pert}
\end{figure}

We formulate these ideas in the adiabatic theorem:
\begin{center}
	\textit{A system with time varying Hamiltonian will remain in its instantaneous ground state as long as the
		evolution is slow enough.}
\end{center}
We map the evolution of the Hamiltonian on the parameter space $s \in [0,1]$. The parameter $s$ gives a generalized
form of time evolution. The Hamiltonian can be always diagonalized for every parameter value
\begin{equation*}
	H(s) = \sum_{j=0}^{D-1} E_j(s) \bra{E_j(s)}\ket{E_j(s)}.
\end{equation*}
For simplicity we demand a unique ground state so that $E_0(s) < E_1(s) \le E_2(s) \le \dots$. In the case of a
degenerated ground state these are of course allowed to mix during this process. In this case our wave function at
$s=0$ reads $\ket{\Psi(0)} = \ket{E_0(0)}$. The adiabatic theorem reads
\begin{equation*}
	\BrKt|<E_0(1)|\Psi(T)>|^2; \to 1,\quad\text{for }T\gg 1.
\end{equation*}
The total runtime for this adiabatic evolution can be computed from
\begin{align*}
	T &\gg \frac{\varepsilon}{\Delta^2},\\
	\intertext{where}
	\varepsilon &= \max_{s\in[0,1]} \BrKt|<E_1(s)|\frac{\partial H(s)}{s}|E_0(s)>|;\\
	\Delta &= \min_{s\in[0,1]} (E_1(s) - E_0(s)).
\end{align*}

\subsection{Grover's search algorithm}
We use the adiabatic theorem now to restudy Grover's search algorithm. We will work with $2^N$ qubits.
Our initial state will be
\begin{equation*}
	\ket{in} = \frac{1}{2^n}\sum_{k=1}^{2^N}\ket{k}
\end{equation*}
and we use the projection Hamiltonian
\begin{equation*}
	H_0 = \mathbb{1} - \bra{in}\ket{in} \overset{N=1}{=}
	\begin{pmatrix}
		0 & 0 \\ 0 & 1
	\end{pmatrix}.
\end{equation*}
We see, that only the initial state $\ket{in}$ has the eigenvalue $0$, while all other eigenstates have the
eigenvalue $1$.

With Grover's search algorithm we want to end in the state $\ket{w}$, so we define
\begin{equation*}
	H_f = \mathbb{1} - \bra{w}\ket{w}
\end{equation*}
and
\begin{equation*}
 	H(t) = \left(1 - \frac{t}{T}\right)H_0 + \frac{t}{T} H_f.
\end{equation*}

We now compute the parameters from the adiabatic theorem. The energy gap during this process is
\begin{equation*}
	\Delta = E_1 - E_0 = \sqrt{1 - 4\left(1 - \frac{1}{N}s(1-s)\right)}
\end{equation*}
where $s = t/T$. For $s = 1/2$ we get the smallest possible gap of $\Delta = 1/\sqrt{N}$.
The bound for the process time $T$ can be written as
\begin{align*}
	\delta &= \frac{\BrKt|<E_1(s)|\frac{\partial H(t)}{\partial t}|E_0(s)>;}{\Delta^2}\\
	&= \frac{\frac{\dd s}{\dd t} \BrKt|<E_1(s)|\frac{\partial H(t)}{\partial t}|E_0(s)>;}{\Delta^2}\\
	&= \frac{N}{T}\underset{\le 1}{\underbrace{\BrKt|<E_1(s)|\frac{\partial H(t)}{\partial t}|E_0(s)>;}}.
\end{align*}
This implies, that $T \le N/\delta$ and since $\delta \ll 1$ we have $T \ge N$, i.e., $T$ scales with $N$.

A stronger bound on $T$ can be achieved by starting at
\begin{equation*}
	\delta = \frac{\frac{\dd s}{\dd t} \BrKt|<E_1(s)|\frac{\partial H(t)}{\partial t}|E_0(s)>;}{\Delta^2}
\end{equation*}
and rearranging this expression to
\begin{equation*}
	\frac{\dd s}{\dd t} \le \Delta^2(s) \delta.
\end{equation*}
By separating the variables we get
\begin{equation*}
	\int_0^1 \frac{\dd s}{1 - 4\left(1 - \frac{1}{N}s(1-s)\right)} \le \delta \int_{0}^{T} \dd t.
\end{equation*}
The first integral can be solved with the help of a integral table
\begin{equation*}
	\int_0^1 \frac{\dd s}{1 - 4\left(1 - \frac{1}{N}s(1-s)\right)}
		= \underset{\approx \frac{\pi}{2}\sqrt{N}}{\underbrace{\frac{N}{2\sqrt{N-1}} \arctan\left(\sqrt{N-1}\right)}}.
\end{equation*}
From this we get the improved bound
\begin{equation*}
	T \ge \frac{\pi}{2\delta}\sqrt{N}.
\end{equation*}
































